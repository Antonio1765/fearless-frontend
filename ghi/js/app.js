window.addEventListener('DOMContentLoaded', async () => {

    function createCard(name, description, pictureUrl, city, startDate, endDate) {
        return `
        <div class="card">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <p class="card-subtitle mb-2 text-muted">${city}</p>
            <p class="card-text">${description}</p>
            </div>
            </div>
            <div class="card-footer">${startDate} - ${endDate}</div>
      `;
    }

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("Response Failed");
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();

              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const city = details.conference.location.city;

              const start = new Date(details.conference.starts);
              const startDay = start.getUTCDate();
              const startMonth = start.getUTCMonth();
              const startYear = start.getUTCFullYear();
              const startDate = `${startMonth}/${startDay}/${startYear}`;

              const end = new Date(details.conference.ends);
              const endDay = end.getUTCDate();
              const endMonth = end.getUTCMonth();
              const endYear = end.getUTCFullYear();
              const endDate = `${endMonth}/${endDay}/${endYear}`;

              const html = createCard(name, description, pictureUrl, city, startDate, endDate);

              const column = document.querySelector('.col');
              column.innerHTML += html;
            }
          }

        }
    } catch (e) {
      const column = document.querySelector('.col');
      column.innerHTML = '<div class="alert alert-warning" role="alert">Failed to fetch data :(</div>'
    }

});
